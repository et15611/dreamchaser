﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Dreamchaser
{
    public class Node
    {
        public Vector2 position;
        public int gScore; //number of points between start and current
        public int hScore; //estimated tiles between current and destination
        public Node parent;
        public Tile tile;

        public Node ()
        {

        }

        public Node(Vector2 position, int gScore, int hScore, Tile tile)
        {
            this.position = position;
            this.gScore = gScore;
            this.hScore = hScore;
            this.tile = tile;
        }

        public Node(Vector2 position, int gScore, int hScore, Node parent, Tile tile)
        {
            this.position = position;
            this.gScore = gScore;
            this.hScore = hScore;
            this.parent = parent;
            this.tile = tile;
        }
    }

    public class AStar
    {
        List<Node> closedList;
        List<Node> openList;
        public List<Node> neighbours;
        List<int> pathLengths;
        int moveCost = 1;
        Vector2 destination = new Vector2();
        List<Vector2> directions;


        void Initialise()
        {
            directions = new List<Vector2>();
            directions.Add(new Vector2(0, -1)); //top
            directions.Add(new Vector2(0, 1)); //bottom
            directions.Add(new Vector2(-1, 0)); //left
            directions.Add(new Vector2(1, 0)); //right

            closedList = new List<Node>();
            openList = new List<Node>();
            Vector2 destination = new Vector2();
            destination.X = (float)Math.Floor(Log.exitPosition.X / Tile.frameSize.X);
            destination.Y = (float)Math.Floor(Log.exitPosition.Y / Tile.frameSize.Y);

            Vector2 position = new Vector2();
            position.X = (float)Math.Floor(Log.exitPosition.X / Tile.frameSize.X);
            position.Y = (float)Math.Floor(Log.exitPosition.Y / Tile.frameSize.Y);

            //Console.WriteLine("Exit = " + Log.exitPosition + " Tile = " + Tile.frameSize);

            openList.Add(new Node(position, 0, ComputeHScore(position), MapManager.map.GetTile(position)));
            openList[0].parent = openList[0];

            pathLengths = new List<int>();
        }


        public void CalculateShortestPath()
        {
            Initialise();
            Node currentStep = new Node();
            while (openList.Count > 0)
            {
                currentStep = openList[0];
                if (currentStep.position == destination)
                {
                    pathLengths.Add(currentStep.gScore);
                }

                closedList.Add(currentStep);
                openList.Remove(currentStep);

                FindNeighbours(currentStep);
            }

            foreach (int i in pathLengths)
            {
                Console.WriteLine("pl = " + i);
            }
            Log.shortestPath = pathLengths.Min();
        }


        bool IsPresentInList (Node element, List<Node> list)
        {
            foreach (Node s in list)
            {
                if (element.tile == s.tile)
                {
                    return true;
                }
            }
            return false;
        }


        void FindNeighbours(Node source)
        {
            Node nextNode;
            neighbours = new List<Node>();
            foreach (Vector2 d in directions)
            {
                if (MapManager.map.GetTile(source.position + d) != null)
                {
                    Tile tempTile = MapManager.map.GetTile(source.position+d);
                    int index = closedList.FindIndex(step => (step.tile == tempTile));

                    Console.WriteLine(tempTile.isWalkable);

                    if (!tempTile.isWalkable)
                    {
                        continue;
                    }

                    if (index >= 0)
                    {
                        nextNode = closedList[index];
                    }
                    else
                    {
                        nextNode = new Node(source.position + d
                    , 0
                    , 0
                    , source, MapManager.map.GetTile(source.position + d));
                    }

                    var gScore = source.gScore+moveCost;
                    var hScore = ComputeHScore(source.position+d);
                    var fScore = gScore + hScore;

                    if (IsPresentInList(nextNode, closedList) && fScore >= FScore(nextNode))
                    {
                        continue;
                    }
                    else if (!IsPresentInList(nextNode, closedList) || fScore < FScore(nextNode))
                    {
                        nextNode.gScore = gScore;
                        nextNode.hScore = hScore;
                        if (!IsPresentInList(nextNode, openList))
                        {
                            openList.Add(nextNode);
                        }
                        openList = openList.OrderBy(node => (node.hScore + node.gScore)).ToList<Node>();
                    }
                }
            }
        }


        /*public void CalculateShortestPath()
        {
            Initialise();
            Step currentStep = new Step();
            bool exitFound = false;
            while (!exitFound) {
                currentStep = openList[0];
                if (currentStep.position == destination)
                {
                    //finished
                    exitFound = true;
                    return;
                }
                closedList.Add(currentStep);
                openList.Remove(currentStep);

                FindNeighbours(currentStep);
                foreach (Step s in neighbours)
                {
                    int closedIndex = closedList.FindIndex(step => (step.position == s.position));
                    int openIndex = openList.FindIndex(step => (step.position == s.position));

                    if (closedIndex >= 0)
                    {
                        /*if (closedList[closedIndex].gScore > s.gScore)
                        {
                            closedList[closedIndex] = s;
                        }
                        else if (s.gScore >= closedList[closedIndex].gScore && openList.FindIndex(step => (step.position == s.position)) >= 0)
                        {
                            s.gScore = closedList[closedIndex].gScore;
                            s.parent = closedList[closedIndex].parent;
                        }*
                        continue;
                    }
                    else if (openIndex < 0)
                    {
                        openList.Add(s);
                    }
                    int tentativeGScore = currentStep.gScore+moveCost;
                    if (tentativeGScore >= s.gScore)
                    {
                        continue;
                    }
                    s.hScore = ComputeHScore(s.position);
                }
                openList.OrderBy(step => step.gScore + step.hScore);
                Console.WriteLine(openList.Count);
            }
        }*
        

        /*public void CalculateShortestPath()
        {
            Initialise();

            Step currentStep = openList[0];

            while (openList.Count != 0)
            {
                Console.WriteLine("Count = " + openList.Count);
                currentStep = openList[0];

                int lowestf = int.MaxValue;
                foreach (Step s in openList)
                {
                    if (FScore(s) < lowestf)
                    {
                        currentStep = s;
                        lowestf = FScore(s);
                    }
                }

                if (currentStep.position == destination)
                {
                    pathFound = true;
                    pathLength = currentStep.gScore;
                    break;
                    //count back to find the path if necessary
                }

                openList.Remove(currentStep);
                closedList.Add(currentStep);

                foreach (Vector2 d in directions)
                {
                    Vector2 currentTile = currentStep.position;
                    Console.WriteLine(currentStep.position);
                    Step step = new Step();
                    step.id = id;
                    id++;
                    step.position = currentStep.position + d;
                    step.gScore = currentStep.gScore + moveCost;
                    step.hScore = ComputeHScore(step.position);
                    int fScore = FScore(currentStep);
                    if (closedList.Contains(step) && fScore >= FScore(step))
                    {
                        continue;
                    }
                    if (!closedList.Contains(step) || fScore < FScore(step))
                    {
                        if (MapManager.map.GetTile(currentTile + d).isWalkable)
                        {
                            Console.WriteLine("Step.position = " + step.position);
                            int index = openList.FindIndex(item => item.position.X == step.position.X && item.position.Y == step.position.Y);

                            if (!(index >= 0))
                            {
                                
                                step.parent = currentStep.position;
                                if (!openList.Contains(step))
                                {
                                    openList.Add(step);
                                }
                            }
                            /*else
                            {
                                step = openList[index];
                                if (currentStep.gScore + moveCost < step.gScore)
                                {
                                    step.gScore = currentStep.gScore + moveCost;
                                }
                            }
                        }
                    }
                }
            } 

            if (!pathFound)
            {
                //MapManager.GenerateNewMap();
            }
            else
            {
                Log.shortestPath = pathLength;
            }
        }*


        int FScore(Node s)
        {
            return s.gScore + s.hScore;
        }


        /*void AddToOpenList(Step s)
        {
            for (int i = 0; i < openList.Count; i++)
            {
                if (FScore(openList[i]) < FScore(s))
                {
                    openList.Insert(i + 1, s);
                    return;
                }
            }
        }*


        int ComputeHScore(Vector2 source)
        {
            return (int)(Math.Abs(source.X - destination.X) + Math.Abs(source.Y - destination.Y));
        }*
    }
}*/
