﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Dreamchaser.MapGenerators
{
    class FloatingIsland
    {
        int WIDTH, HEIGHT;
        Random rnd;
        List<List<int>> tiles;
        List<Vector2> corners;
        List<Room> rooms;


        public FloatingIsland(int width, int height, Random rnd)
        {
            WIDTH = width;
            HEIGHT = height;
            this.rnd = rnd;
            tiles = new List<List<int>>();
            corners = new List<Vector2>();
            rooms = new List<Room>();
            InitialiseMap();
        }


        void InitialiseMap ()
        {
            for (int height = 0; height < HEIGHT; height++)
            {
                tiles.Add(new List<int>());
                for (int width = 0; width < WIDTH; width++)
                {
                    if (height == 0 || height == HEIGHT - 1 || width == 0 || width == WIDTH - 1)
                    {
                        tiles[height].Add(0);
                    }
                    else
                    {
                        tiles[height].Add(1);
                    }
                }
            }
        }


        void DrawPath (Room room1, Room room2)
        {
            if (room1.Centre().X - room2.Centre().X >= 0)
            {
                for (int x = 0; x < room1.Centre().X - room2.Centre().X; x++)
                {
                    tiles[(int)(room1.Centre().X - x)][(int)(room1.Centre().Y)] = 2;
                }
            }
            else 
            {
                for (int x = 0; x < room2.Centre().X - room1.Centre().X; x++)
                {
                    tiles[(int)(room2.Centre().X - x)][(int)(room2.Centre().Y)] = 2;
                }
            }


            if (room1.Centre().Y - room2.Centre().Y >= 0)
            {
                for (int y = 0; y < room1.Centre().Y - room2.Centre().Y; y++)
                {
                    tiles[(int)(room1.Centre().X)][(int)(room1.Centre().Y - y)] = 2;
                }
            }
            else
            {
                for (int y = 0; y < room2.Centre().Y - room1.Centre().Y; y++)
                {
                    tiles[(int)(room2.Centre().X)][(int)(room2.Centre().Y - y)] = 2;
                }
            }
        }


        void GeneratePaths ()
        {

            for (int i = 0; i < rooms.Count(); i++)
            {
                DrawPath(rooms[i], rooms[(i+1)%rooms.Count]);//rooms[rnd.Next(0,rooms.Count())]);
            } 
        }


        void CreateRooms ()
        {
            foreach (Vector2 upperLeft in corners)
            {
                Console.WriteLine(upperLeft);
                bool xFound = false, yFound = false;
                Vector2 lowerRight = new Vector2();
                for (int i=0; !(xFound && yFound); i++)
                {
                    Console.WriteLine("X + i = " + (upperLeft.X+i) + "     Y + i = " + (upperLeft.Y + i));
                    if (!xFound && tiles[(int)(upperLeft.X)+i][(int)(upperLeft.Y)] == 0)
                    {
                        lowerRight.X = upperLeft.X + i;
                        xFound = true;
                    }
                    if (!yFound && tiles[(int)(upperLeft.X)][(int)(upperLeft.Y)+i] == 0)
                    {
                        lowerRight.Y = upperLeft.Y + i;
                        yFound = true;
                    }
                }
                rooms.Add(new Room(lowerRight, upperLeft));
            }
        }


        void BisectHorizontally ()
        {
            int startingPoint = rnd.Next((int)(HEIGHT * 0.3), (int)(HEIGHT * 0.7));
            int lastWallX = 1;
            for (int i = 0; i < WIDTH; i++)
            {
                if (tiles[i][startingPoint] == 0 && i != 0)
                {
                    corners.Add(new Vector2(lastWallX, startingPoint+1));
                    lastWallX = i;
                    startingPoint = rnd.Next((int)(HEIGHT*0.3), (int)(HEIGHT*0.7));
                }
                else 
                {
                    tiles[i][startingPoint] = 0;
                }
            }
        }


        void BisectVertically()
        {
            int startingPoint = rnd.Next((int)(WIDTH * 0.3), (int)(WIDTH * 0.7));
            int lastWallY = 1;
            for (int i=0; i < HEIGHT; i++)
            {
                if (tiles[startingPoint][i] == 0 && i != 0)
                {
                    corners.Add(new Vector2(startingPoint+1, lastWallY));
                    lastWallY = i;
                    startingPoint = rnd.Next((int)(WIDTH*0.3), (int)(WIDTH*0.7));
                } 
                else 
                {
                    tiles[startingPoint][i] = 0;
                }
            }
        }


        public List<List<int>> GenerateMap()
        {
            corners.Add(new Vector2(1, 1));
            for (int i = 0; i<2; i++)
            {
                BisectHorizontally();
                BisectVertically();
            }
            CreateRooms();
            GeneratePaths();
            return tiles;
        }
    }
}*/
