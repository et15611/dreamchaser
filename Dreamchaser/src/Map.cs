﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dreamchaser
{
    public class Map
    {
        public static Random rnd;
        public List<List<Tile>> tiles;          // The map which eventually gets output 
        public List<List<int>> tempMap;         // Contain the map returned from the generators
        public static int WIDTH = 20, HEIGHT = 20;
        MapGenerators.CellularAutomata cellularAutomata;
        MapGenerators.PerlinNoise      perlinNoise;
        MapGenerators.DungeonGenerator dungeonGenerator;


        public Map()
        {
            tiles = new List<List<Tile>>();
        }


        public void Load() // Initialise variables and reset applicable values in the log.
        {
            rnd = new Random();
            tempMap = new List<List<int>>();
            RandomiseMap();
            ConvertToTiles(tempMap);
            SpriteManager.GenerateEnemies(rnd);
            foreach (Character c in SpriteManager.enemies)
                c.AssignPosition(GenerateValidPosition());
            GameManager.player.AssignPosition(GenerateValidPosition());
            SpriteManager.entities[0].AssignPosition(GenerateValidPosition());

            Log.playerStart = GameManager.player.Position();
            Log.exitPosition = SpriteManager.entities[0].position;
            Log.stepsTaken = 1;
            Log.CalcManhattanDistance();
            Log.FindShortestPath();
        }


        void RandomiseMap() // Select which generator to use, and create the new map.
        {

            HEIGHT = 15 * MapManager.mapSize + 15;
            WIDTH = HEIGHT;

            
            if (MapManager.mapType == 0)
            {
                dungeonGenerator = new MapGenerators.DungeonGenerator();
                tempMap = dungeonGenerator.GenerateMap();
            }
            else if (MapManager.mapType == 1)
            {
                perlinNoise = new MapGenerators.PerlinNoise();
                tempMap = perlinNoise.GenerateMap();
            }
            else
            {
                cellularAutomata = new MapGenerators.CellularAutomata();
                tempMap = cellularAutomata.GenerateMap();
            }
        }


        public Tile GetTile (Vector2 position)  // Allow other classes to safely access the map
        {
            if (position.X >= WIDTH || position.Y >= HEIGHT || position.Y < 0 || position.X < 0)
                return null;
            return tiles[(int)position.X][(int)position.Y];
        }


        void ConvertToTiles(List<List<int>> generated) // Convert between the tempMap and map variables
        {
            for (int height = 0; height < HEIGHT; height++)
            {
                tiles.Add(new List<Tile>());
                for (int width = 0; width < WIDTH; width++)
                {
                    Tile newTile = new Tile(new Vector2(width, height), true);
                    Tile wallTile = new Tile(new Vector2(width, height), false);
                    Tile pathTile = new Tile(new Vector2(width, height), true, 0);
                    tiles[height].Add((generated[height][width] == 2) ? pathTile : (generated[height][width] == 1) ? newTile : wallTile);
                    Console.Write(tiles[height][width]==wallTile ? "0" : ".");
                }
                Console.WriteLine(" ");
            }
            Console.WriteLine(" ");
        }


        public Vector2 GenerateValidPosition() // Generate a valid position for a character or map feature.
        {
            Vector2 point = new Vector2(rnd.Next(0, WIDTH), rnd.Next(0, HEIGHT));

            while (true)
            {
                point = new Vector2(rnd.Next(0, WIDTH), rnd.Next(0, HEIGHT));
                if (tiles[(int)point.Y][(int)point.X].isSpawnable == true && GetObjectAtPosition(point*Tile.frameSize.X) == null)
                {
                    return point*Tile.frameSize.X;
                }
            }
        }


        public Character GetObjectAtPosition (Vector2 position) // Check for objects at some point - used for collisions
        {
            Character objects = null;
            foreach (Character c in SpriteManager.enemies)
            {
                if (c.position == position)
                {
                    objects = c;
                } 
            }
            foreach (Character e in SpriteManager.entities)
            {
                if (e.position == position)
                {
                    objects = e;
                }
            }
            return objects;
        }


        public bool IsWalkable (Vector2 position) // Checks whether characters can walk on some space
        {            
            return tiles[(int)(position.X/48)][(int)(position.Y/48)].isWalkable;
        }


        public void Draw(GameTime gametime, SpriteBatch spriteBatch)
        {
            for (int height = 0; height < HEIGHT; height++)
            {
                for (int width = 0; width < WIDTH; width++)
                {
                   tiles[height][width].Draw(gametime, spriteBatch);
                }
            }
        }
    }
}

