﻿using System;
using System.IO;

namespace Dreamchaser
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string fileName = "TestResults.txt";
            using (StreamWriter sw = new StreamWriter((fileName).ToString(), false))
            {
                sw.WriteLine();
            }

            using (var game = new GameManager())
                game.Run();
        }
    }
#endif
}

