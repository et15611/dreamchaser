﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Dreamchaser
{
    public abstract class Character
    {
        public bool isSorted = false;
        public Vector2 position;

        public abstract void Interact();

        public abstract void Draw(GameTime gametime, SpriteBatch spriteBatch);
        
        public abstract void AssignPosition(Vector2 position);
    }
}
