﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dreamchaser
{
    public enum States { IDLE, IDLE_S, WALKING, ATTACKING, HURT, DYING };
    public enum Directions { UP, DOWN, LEFT, RIGHT, NONE };

    class Animation
    {
        public States state;
        public Directions direction;
        public int frames = 0;
        private int startFrame;
        public int currentFrame;
        bool changedStates;
        bool flipX;
        const int frameTime = 90; // time in milliseconds before the frame increments
        int timeSinceLastFrame = 0;


        public Animation ()
        {
            state = States.IDLE;
            currentFrame = 0; 
        }

        public Animation(States state, int frames, Directions direction, int startFrame, bool flipX = false)
        {
            changedStates = false;
            if (this.state != state || this.direction != direction)
            {
                changedStates = true;
            }
            this.state = state;
            this.direction = direction;
            this.frames = frames;
            this.flipX = flipX;
            this.startFrame = startFrame;
        }

        public static int FrameTime ()
        {
            return frameTime;
        }

        public void IncrementFrame (GameTime gameTime)
        {
            timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;

            if (changedStates)
            {
                currentFrame = 0;
                changedStates = false;
            }

            if (timeSinceLastFrame > frameTime)
            {
                timeSinceLastFrame = 0;
                currentFrame = (currentFrame + 1) % frames;
            }
        }

        public void Draw (GameTime gametime, SpriteBatch spriteBatch, Texture2D texture, Vector2 position, Point frameSize)
        {
            int framesX = texture.Width / frameSize.X;                                          // total number of frames in x and y
            int framesY = texture.Height / frameSize.Y;
            int x = ((startFrame + currentFrame) % framesX) * frameSize.X;
            int y = ((startFrame + currentFrame) / framesX) * frameSize.Y;
            Point framePosition = new Point(x, y);                                              // the starting position to feed into spriteBatch.Draw
            SpriteEffects flip = flipX ? SpriteEffects.FlipHorizontally : SpriteEffects.None;   // if flip is true, flip the sprite horizontally. 

            spriteBatch.Draw(texture, position, new Rectangle((framePosition.X), (framePosition.Y), frameSize.X, frameSize.Y), Color.White, 0, Vector2.Zero, 1, flip, 0);
        }
    }
}
