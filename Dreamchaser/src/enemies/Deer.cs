﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Dreamchaser.enemies
{
    public class Deer : Character
    {
        private List<Animation> animations;
        public Texture2D texture;
        protected int speed;
        States state;
        public Directions direction;
        public Point frameSize;
        Vector2 initialOffset;
        public Vector2 SpeedInput
        {
            get { return InputManager.Direction; }
        }


        public Deer(Game game)
        {
            texture = game.Content.Load<Texture2D>("deer");
            speed = 6;
            position = new Vector2 (0,0);
            animations = new List<Animation>();
            state = States.IDLE;
            direction = Directions.DOWN;
            initialOffset = new Vector2(-6, -20);
            frameSize = new Point(64, 64);
            animations.Add(new Animation(States.IDLE, 7, Directions.DOWN, 0)); //idle down
            animations.Add(new Animation(States.IDLE, 7, Directions.UP, 84)); //idle up
            animations.Add(new Animation(States.IDLE, 7, Directions.LEFT, 42)); //idle left
            animations.Add(new Animation(States.IDLE, 7, Directions.RIGHT, 42, true)); //idle right
            animations.Add(new Animation(States.IDLE_S, 7, Directions.DOWN, 7)); //special idle down
            animations.Add(new Animation(States.IDLE_S, 7, Directions.UP, 91)); //special idle up
            animations.Add(new Animation(States.IDLE_S, 7, Directions.LEFT, 49)); //special idle left
            animations.Add(new Animation(States.IDLE_S, 7, Directions.RIGHT, 49, true)); //special idle right
            animations.Add(new Animation(States.WALKING, 7, Directions.DOWN, 21)); //walking down
            animations.Add(new Animation(States.WALKING, 7, Directions.UP, 98)); //walking up
            animations.Add(new Animation(States.WALKING, 7, Directions.LEFT, 53)); //walking left
            animations.Add(new Animation(States.WALKING, 7, Directions.RIGHT, 53, true)); //walking right
            animations.Add(new Animation(States.ATTACKING, 7, Directions.DOWN, 21)); //attacking down
            animations.Add(new Animation(States.ATTACKING, 7, Directions.UP, 105)); //attacking up
            animations.Add(new Animation(States.ATTACKING, 7, Directions.LEFT, 63)); //attacking left
            animations.Add(new Animation(States.ATTACKING, 7, Directions.RIGHT, 63, true)); //attacking right
            animations.Add(new Animation(States.HURT, 7, Directions.DOWN, 28)); //hurt down
            animations.Add(new Animation(States.HURT, 7, Directions.UP, 112)); //hurt up
            animations.Add(new Animation(States.HURT, 7, Directions.LEFT, 70)); //hurt left
            animations.Add(new Animation(States.HURT, 7, Directions.RIGHT, 70, true)); //hurt right
            animations.Add(new Animation(States.DYING, 7, Directions.DOWN, 35)); //dying down
            animations.Add(new Animation(States.DYING, 7, Directions.UP, 119)); //dying up
            animations.Add(new Animation(States.DYING, 7, Directions.LEFT, 77)); //dying left
            animations.Add(new Animation(States.DYING, 7, Directions.RIGHT, 77, true)); //dying right
        }

        public override void AssignPosition(Vector2 position)
        {
            this.position = position;
        }

        public override void Draw(GameTime gametime, SpriteBatch spriteBatch)
        {
            animations.Find(a => a.state == state && a.direction == direction).Draw(gametime, spriteBatch, texture, position+initialOffset, frameSize);
        }


        public void Update(GameTime gameTime, Rectangle clientBounds)
        {
            animations.Find(a => a.state == state && a.direction == direction).IncrementFrame(gameTime);
        }

        public override void Interact()
        {
            state = States.DYING;
        }
    }
}
