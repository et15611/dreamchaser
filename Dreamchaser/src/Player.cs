﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;


namespace Dreamchaser { 

    public class Player : Character
    {
        private List<Animation> animations;
        public Texture2D texture;
        protected float speed;
        States state;
        public Directions direction;
        public Point frameSize;
        Vector2 previousPosition, nextPosition, initialOffset;
        float animPercent;
        bool specialAnimation;

        public Vector2 SpeedInput
        {
            get { return InputManager.Direction; }
        }

        public Vector2 Position ()
        {
            return position;
        }


        public Player(Game game)
        {
            specialAnimation = false;
            texture = game.Content.Load<Texture2D>("zarrinBear");
            speed = 6;
            initialOffset = new Vector2(-6,-20);
            position = new Vector2();
            animations = new List<Animation>();
            state = States.IDLE;
            direction = Directions.DOWN;
            frameSize = new Point(64, 64);
            animations.Add(new Animation(States.IDLE, 7, Directions.DOWN, 0)); //idle down
            animations.Add(new Animation(States.IDLE, 7, Directions.UP, 63)); //idle up
            animations.Add(new Animation(States.IDLE, 7, Directions.LEFT, 35, true)); //idle left
            animations.Add(new Animation(States.IDLE, 7, Directions.RIGHT, 35)); //idle right
            animations.Add(new Animation(States.IDLE_S, 14, Directions.DOWN, 7)); //special idle down
            animations.Add(new Animation(States.IDLE_S, 14, Directions.UP, 70)); //special idle up
            animations.Add(new Animation(States.IDLE_S, 14, Directions.LEFT, 56, true)); //special idle left
            animations.Add(new Animation(States.IDLE_S, 14, Directions.RIGHT, 56)); //special idle right
            animations.Add(new Animation(States.WALKING, 7, Directions.DOWN, 21)); //walking down
            animations.Add(new Animation(States.WALKING, 7, Directions.UP, 84)); //walking up
            animations.Add(new Animation(States.WALKING, 7, Directions.LEFT, 49, true)); //walking left
            animations.Add(new Animation(States.WALKING, 7, Directions.RIGHT, 49)); //walking right
            animations.Add(new Animation(States.ATTACKING, 7, Directions.DOWN, 28)); //attacking down
            animations.Add(new Animation(States.ATTACKING, 7, Directions.UP, 88)); //attacking up
            animations.Add(new Animation(States.ATTACKING, 7, Directions.LEFT, 56, true)); //attacking left
            animations.Add(new Animation(States.ATTACKING, 7, Directions.RIGHT, 56)); //attacking right
            animations.Add(new Animation(States.HURT, 7, Directions.DOWN, 28)); //hurt down
            animations.Add(new Animation(States.HURT, 7, Directions.UP, 112)); //hurt up
            animations.Add(new Animation(States.HURT, 7, Directions.LEFT, 70)); //hurt left
            animations.Add(new Animation(States.HURT, 7, Directions.RIGHT, 70, true)); //hurt right
        }

        public override void AssignPosition (Vector2 position)
        {
            this.position = position;
            previousPosition = this.position;
            nextPosition = this.position;
        }

        public override void Draw (GameTime gametime, SpriteBatch spriteBatch)
        {
            animations.Find(a => a.state==state && a.direction==direction).Draw(gametime, spriteBatch, texture, position+initialOffset, frameSize);
            
        }


        public void Update(GameTime gameTime, Rectangle clientBounds)
        {
            
            if (animPercent == 1)
            {
                previousPosition = position;
                if (SpeedInput.X > 0)
                {
                    direction = Directions.RIGHT;
                    state = States.WALKING;
                    animPercent = 0;
                }
                else if (SpeedInput.X < 0)
                {
                    direction = Directions.LEFT;
                    state = States.WALKING;
                    animPercent = 0;
                }
                else if (SpeedInput.Y > 0)
                {
                    direction = Directions.DOWN;
                    state = States.WALKING;
                    animPercent = 0;
                }
                else if (SpeedInput.Y < 0)
                {
                    direction = Directions.UP;
                    state = States.WALKING;
                    animPercent = 0;
                }
                else if (InputManager.isAttacking)
                {
                    state = States.ATTACKING;
                    specialAnimation = true;
                    animPercent = 0;
                }
                else
                {
                    state = States.IDLE;
                }
                Vector2 projectedPosition = new Vector2(previousPosition.Y + Tile.FrameSize().Y * SpeedInput.Y, previousPosition.X + Tile.FrameSize().X * SpeedInput.X);
                
                if (MapManager.map.IsWalkable(projectedPosition))
                { 
                    nextPosition = new Vector2(projectedPosition.Y, projectedPosition.X);
                    if (MapManager.map.GetObjectAtPosition(nextPosition) != null)
                    {
                        MapManager.map.GetObjectAtPosition(nextPosition).Interact();
                        nextPosition = previousPosition;
                    }
                }
                else
                {
                    nextPosition = previousPosition;
                }
            }
            else
            {
                if (specialAnimation)
                {
                    animPercent += ((float)gameTime.ElapsedGameTime.Milliseconds)/(7*Animation.FrameTime());
                    if (animPercent >= 1)
                    {
                        animPercent = 1;
                        specialAnimation = false;
                    }
                }
                else
                {
                    animPercent += ((speed * (float)gameTime.ElapsedGameTime.Milliseconds) / 700);
                    if (animPercent >= 1)
                    {
                        if (previousPosition != nextPosition)
                            Log.stepsTaken++;
                        animPercent = 1;
                    }
                    position = previousPosition + (nextPosition - previousPosition) * animPercent;
                }

            }
            animations.Find(a => a.state == state && a.direction == direction).IncrementFrame(gameTime);
        }


        public override void Interact()
        {
            throw new NotImplementedException();
        }
    }
}
