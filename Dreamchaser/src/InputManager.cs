﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace Dreamchaser
{
    public static class InputManager
    {
        public static Vector2 Direction
        {
            get
            {
                Vector2 inputDirection = Vector2.Zero;
                KeyboardState keyboard = Keyboard.GetState();

                if (keyboard.IsKeyDown(Keys.A))
                {
                    inputDirection.X = -1;
                }
                else if (keyboard.IsKeyDown(Keys.D))
                {
                    inputDirection.X = 1;
                }
                else if (keyboard.IsKeyDown(Keys.S))
                {
                    inputDirection.Y = 1;
                }
                else if (keyboard.IsKeyDown(Keys.W))
                {
                    inputDirection.Y = -1;
                }

                return inputDirection;
            }
        }

        public static bool isAttacking
        {
            get
            {
                KeyboardState keyboard = Keyboard.GetState();
                if (keyboard.IsKeyDown(Keys.Space))
                {
                    return true;
                }
                return false;
            }
        }

    }
}
