﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamchaser
{
    public class MapManager
    {
        public static Map map;
        public static int mapSize = 0, mapType = 0, iteration = 0;

        public MapManager() // Initialises
        {
            GenerateNewMap();
        }

        public static void GenerateNewMap () // Creates a new map and iterates the type and size
        {
            if (mapSize == 5)
            {
                GameManager.isExit = true;
            }
            map = new Map();
            map.Load();
            mapType = (mapType + 1);
            if (mapType == 3)
            {
                mapType = 0;
                mapSize++;
            }
        }

        public void Draw(GameTime gametime, SpriteBatch spriteBatch) 
        {
            map.Draw(gametime, spriteBatch);
        }
    }
}
