﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dreamchaser.enemies;
using Dreamchaser.InteractableObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Dreamchaser
{
    public partial class SpriteManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        Player player;
        public static List<Character> enemies;
        public static List<Character> entities;
        Camera camera;
        static Game game;

        public SpriteManager(Game game_) : base(game)
        {
            player = GameManager.player;
            game = game_;
            entities = new List<Character>();
            entities.Add(new Exit());
            camera = new Camera();
        }

        public static void GenerateEnemies(Random rnd)
        {
            enemies = new List<Character>();
            for (int i = 0; i < rnd.Next(3, 8); i++)
            {
                enemies.Add(new Deer(game));
            }
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(game.GraphicsDevice);

            base.LoadContent();
        }

        public static Texture2D ImportTextureByName (string name)
        {
            return game.Content.Load<Texture2D>(name);
        }

        public override void Update(GameTime gameTime)
        {
            player.Update(gameTime, game.Window.ClientBounds);
            foreach (Deer deer in enemies)
            {
                deer.Update(gameTime, game.Window.ClientBounds);
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            List<Character> chars = new List<Character>();
            chars.Add(player);
            foreach (Deer deer in enemies)
            {
                chars.Add(deer);
            }
            foreach (Character entity in entities)
            {
                chars.Add(entity);
            }
            chars = SortCharacters(chars);
            camera.Follow(player);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, transformMatrix: camera.Transform);
            GameManager.mapManager.Draw(gameTime, spriteBatch);
            foreach (Character c in chars)
            {
                c.Draw(gameTime, spriteBatch);
            }
            base.Draw(gameTime);
            spriteBatch.End();
            ResetSorted();
        }

        public List<Character> SortCharacters (List<Character> chars)
        {
            List<Character> sorted = new List<Character>();
            Character highest = null;
            while (sorted.Count != chars.Count)
            {
                foreach (Character c in chars)
                {
                    if (highest == null && !c.isSorted) {
                        highest = c;
                    }
                    else
                    {
                        if (!c.isSorted && c.position.Y < highest.position.Y)
                        {
                            highest = c;
                        }
                    }
                }
                highest.isSorted = true;
                sorted.Add(highest);
                highest = null; 
            }
            return sorted;
        }

        public void ResetSorted ()
        {
            foreach (Character entity in entities)
            {
                entity.isSorted = false;
            }
            foreach (Deer deer in enemies)
            {
                deer.isSorted = false;
            }
            player.isSorted = false;
        }
    }
}
