﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Dreamchaser
{
    class Camera
    {
        public Matrix Transform { get; private set; }

        public void Follow(Player target)
        {
            Matrix position = Matrix.CreateTranslation(-target.position.X - (target.frameSize.X / 2)
                                               , -target.position.Y - (target.frameSize.Y / 2)
                                               , 0);
            Matrix offset = Matrix.CreateTranslation(GameManager.ScreenWidth / 2, GameManager.ScreenHeight / 2, 0);
            Transform = position * offset;
        }
    }
}
