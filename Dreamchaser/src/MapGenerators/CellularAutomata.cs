﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamchaser.MapGenerators
{
    public class CellularAutomata
    {
        List<List<int>> map;      // Two maps, used interchangably as the cellular automata iterates.
        List<List<int>> map_;
        Random rnd;             
        const int density = 65;   // The % threshold at which values are rounded
        int currentIteration;
        const int generations = 5;

        public CellularAutomata() // Run the algorithm
        {
            map = new List<List<int>>();
            map_ = new List<List<int>>();
            this.rnd = Map.rnd;
            currentIteration = 0;
            InitialiseMap();   
        }

        void InitialiseMap()  // Populate an empty map with random values
        {
            for (int height = 0; height < Map.HEIGHT; height++)
            {
                map_.Add(new List<int>());
                for (int width = 0; width < Map.WIDTH; width++)
                {
                    if (height == 0 || height == Map.HEIGHT - 1 || width == 0 || width == Map.WIDTH - 1)
                    {
                        map_[height].Add(0);
                    }
                    else
                    {
                        map_[height].Add((rnd.Next(0, 100) < density) ? 1 : 0);
                    }
                }
            }
            map = map_;
        }


        public List<List<int>> GenerateMap()  // Controls the SmoothMap iterations
        {
            for (int i = 0; i < generations; i++)
            {
                SmoothMap();
                currentIteration++;
            }
            return map;
        }


        void SmoothMap ()  // Executes the rules of the cellular automata
        {
            List<List<int>> newTiles = map; 
            int neighbouringWalls = 0;
            for (int x = 0; x < Map.WIDTH; x++)
            {
                for (int y = 0; y < Map.HEIGHT; y++)
                {
                    neighbouringWalls = CountSurroundingWalls(x, y);

                    if (currentIteration < 4)
                    {
                        if (neighbouringWalls > 4 || neighbouringWalls <= 0)
                        {
                            newTiles[y][x] = 0;
                        }
                        else if (neighbouringWalls < 4)
                        {
                            newTiles[y][x] = 1;
                        }
                    }
                    else
                    {
                        if (neighbouringWalls > 4)
                        {
                            newTiles[y][x] = 0;
                        }
                        else if (neighbouringWalls < 4)
                        {
                            newTiles[y][x] = 1;
                        }
                    }
                }
            }
            map = newTiles;
        }

        int CountSurroundingWalls (int x, int y)  // Counts the number of walls (0s) around some point.
        {
            int wallCount = 0;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (x+i >= 0 && x+i < Map.WIDTH && y+j >= 0 && y+j < Map.HEIGHT)
                    {
                        wallCount += (map[y+j][x+i] == 0) ? 1 : 0;
                    }
                    else
                    {
                        wallCount += 1;
                    }
                }
            }
            return wallCount;
        }
    }
}
