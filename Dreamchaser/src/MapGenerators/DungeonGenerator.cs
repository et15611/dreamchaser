﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Dreamchaser.MapGenerators
{
    class Room
    {
        public Vector2 centre;
        public int height, width;
        Random rnd;
        public bool isConnected; // Has this room already been randomly connected to another?

        public Room(Vector2 centre, int height, int width)
        {
            this.height = height;
            this.width = width;
            this.centre = centre;
            rnd = Map.rnd;
            isConnected = false;
        }


        public void Delete() // Handles "removing" rooms from the list by nullifing the parameters
        {
            height = 0;
            width = 0;
            centre = new Vector2(0, 0);
        }
    }

    class DungeonGenerator
    {
        List<Room> rooms;
        List<List<int>> tiles;
        List<int> idUsed;
        Random rnd;
        private int[] sizes = { 5, 7, 9, 11 };


        public DungeonGenerator() // Handles the generation logic
        {
            tiles = new List<List<int>>();
            rooms = new List<Room>();
            this.rnd = Map.rnd;
            idUsed = new List<int>();
            InitialiseMap();
        }


        void InitialiseMap() // Populates a new list<lists> with 0s
        {
            for (int height = 0; height < Map.HEIGHT; height++)
            {
                tiles.Add(new List<int>());
                for (int width = 0; width < Map.WIDTH; width++)
                {
                    tiles[height].Add(0);
                }
            }
        }


        public List<List<int>> GenerateMap() //Handles generating the map itself
        {
            GenerateRooms();
            PlaceRooms();
            ConnectRooms();
            CleanTiles();
            return tiles;
        }


        void GenerateRooms() // Creates a list of rooms
        {
            float coverage = 1f;
            int totalTiles = Map.HEIGHT * Map.WIDTH;
            int areaGenerated = 0;
            int i = 0;

            while (areaGenerated < Math.Round((double)totalTiles * coverage))
            {
                rooms.Add(MakeARoom());
                areaGenerated += rooms[i].height * rooms[i].width;
                i++;
            }

            rooms.Sort((a, b) => (a.width * a.height).CompareTo(b.width * b.height));
            rooms.Reverse();
        }


        Room MakeARoom() // Creates a single new room and populates all the parameters
        {
            Vector2 centrePoint = new Vector2();
            centrePoint.X = rnd.Next(0, (Map.WIDTH - 2) / 2) * 2 + 1;
            centrePoint.Y = rnd.Next(0, (Map.HEIGHT - 2) / 2) * 2 + 1;
            int height = sizes[rnd.Next(0, sizes.Length)];
            int width = sizes[rnd.Next(0, sizes.Length)];
            Room room = new Room(centrePoint, height, width);
            return room;
        }


        void PlaceRooms() // Places rooms on the map and handles collisions
        {
            List<List<int>> tempMap = new List<List<int>>();
            for (int height = 0; height < Map.HEIGHT; height++)
            {
                tempMap.Add(new List<int>());
                for (int width = 0; width < Map.WIDTH; width++)
                {
                    tempMap[height].Add(0);
                }
            }

            CopyMatrices(tiles, tempMap);

            foreach (Room r in rooms)
            {
                int _topX = (int)r.centre.X - r.width / 2, _topY = (int)r.centre.Y - r.height / 2, x = 0, y = 0;
                int topX = Math.Min(Math.Max(_topX, 1), Map.WIDTH - r.width - 1);
                int topY = Math.Min(Math.Max(_topY, 1), Map.HEIGHT - r.height - 1);
                r.centre.X = (int)Math.Ceiling((double)(topX + r.width / 2));
                r.centre.Y = (int)Math.Ceiling((double)(topY + r.height / 2));
                bool overlapping = false;


                for (int i = -1; i <= r.width; i++)
                {
                    x = topX + i;
                    for (int j = -1; j <= r.height; j++)
                    {
                        y = topY + j;
                        if (i == -1 || j == -1 || i == r.width || j == r.height)
                        { tempMap[y][x] = -1; }

                        else if (tiles[y][x] == 0)
                        { tempMap[y][x] = 1; }

                        else
                        { overlapping = true; }
                    }
                }
                if (!overlapping) { CopyMatrices(tempMap, tiles); }
                else
                {
                    CopyMatrices(tiles, tempMap);
                    r.Delete();
                }
            }
        }


        void CopyMatrices(List<List<int>> source, List<List<int>> target) // A method of copying matrices to ensure they were copied by value and not reference
        {
            for (int i = 0; i < source.Count; i++)
            {
                for (int j = 0; j < source[i].Count; j++)
                {
                    int x = source[i][j];
                    target[i][j] = x;
                }
            }
        }


        void ConnectRooms() // Adds in the paths between rooms
        {
            foreach (Room r in rooms)
            {
                if (r.height != 0)
                {
                    int roomID = -1;
                    int backupID = -1;
                    float distance = float.MaxValue;
                    for (int j = 0; j < rooms.Count; j++)
                    {
                        if (rooms[j] != r && rooms[j].height != 0 && distance > Math.Sqrt(Math.Pow(rooms[j].centre.X - r.centre.X, 2) + Math.Pow(rooms[j].centre.Y - r.centre.Y, 2)))
                        {
                            if (!idUsed.Contains(j))
                            {
                                distance = (float)Math.Sqrt(Math.Pow(rooms[j].centre.X - r.centre.X, 2) + Math.Pow(rooms[j].centre.Y - r.centre.Y, 2));
                                roomID = j;
                                idUsed.Add(j);
                            }
                            backupID = j;
                        }
                    }
                    if (roomID == -1)
                        roomID = backupID;
                    if (roomID == -1)
                        roomID = 0;
                    DrawPath(r, rooms[roomID]);
                }
            }
        }


        void DrawPath(Room a, Room b) // Draws a single path between two points
        {
            Vector2 xLargest = new Vector2(), xSmallest = new Vector2();
            Vector2 yLargest = new Vector2(), ySmallest = new Vector2();
            if (a.centre.X < b.centre.X)
            {
                xLargest = b.centre;
                xSmallest = a.centre;
            }
            else
            {
                xLargest = a.centre;
                xSmallest = b.centre;
            }
            if (a.centre.Y < b.centre.Y)
            {
                yLargest = b.centre;
                ySmallest = a.centre;
            }
            else
            {
                yLargest = a.centre;
                ySmallest = b.centre;
            }
            for (var x = (int)xSmallest.X; x <= (int)xLargest.X; x++)
            {
                tiles[(int)xSmallest.Y][x] = 2;
            }
            for (var y = (int)ySmallest.Y; y <= (int)yLargest.Y; y++)
            {
                tiles[y][(int)xLargest.X] = 2;
            }
        }


        void CleanTiles() // Removes the placeholder -1 tiles.
        {
            for (int height = 0; height < Map.HEIGHT; height++)
            {
                for (int width = 0; width < Map.WIDTH; width++)
                {
                    if (tiles[height][width] == -1)
                    {
                        tiles[height][width] = 0;
                    }
                }
            }
        }
    }
}
