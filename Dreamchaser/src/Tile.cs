﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Dreamchaser
{
    public class Tile
    {
        private List<Animation> animations;
        public Texture2D texture;
        States state;
        public Directions direction;
        public static Point frameSize;
        Vector2 position;
        public bool isWalkable;
        public bool isSpawnable;


        public Tile (Vector2 pos, bool isWalkable)
        {
            texture = SpriteManager.ImportTextureByName("floortiles");
            animations = new List<Animation>();
            frameSize = new Point(48, 48);
            state = States.IDLE;
            direction = Directions.DOWN;
            animations.Add(new Animation(States.IDLE, 1, Directions.DOWN, 0)); //idle down
            position = pos * frameSize.ToVector2();
            this.isWalkable = isWalkable;
            if (isWalkable)
                isSpawnable = true;
            else
                isSpawnable = false;
        }

        public Tile (Vector2 pos, bool isWalkable, int frame)
        {
            texture = SpriteManager.ImportTextureByName("floortiles");
            animations = new List<Animation>();
            frameSize = new Point(48, 48);
            state = States.IDLE;
            direction = Directions.DOWN;
            animations.Add(new Animation(States.IDLE, 1, Directions.DOWN, frame)); //idle down
            position = pos * frameSize.ToVector2();
            this.isWalkable = isWalkable;
            isSpawnable = false;
        }
        
        public static Vector2 FrameSize()
        {
            return frameSize.ToVector2();
        }

        public Tile ()
        {

        }

        public void Draw(GameTime gametime, SpriteBatch spriteBatch)
        {
            if (isWalkable)
            {
                animations.Find(a => a.state == state && a.direction == direction).Draw(gametime, spriteBatch, texture, position, frameSize);
            }
        }
    }
}
