﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dreamchaser.InteractableObjects
{
    class Exit : Character
    {
        private List<Animation> animations;
        public Texture2D texture;
        States state;
        public Directions direction;
        public static Point frameSize;


        public Exit()
        {
            texture = SpriteManager.ImportTextureByName("exitPortal");
            animations = new List<Animation>();
            frameSize = new Point(48, 48);
            state = States.IDLE;
            direction = Directions.DOWN;
            animations.Add(new Animation(States.IDLE, 1, Directions.DOWN, 0)); //idle down
            position = new Vector2(0, 0);
        }


        public override void AssignPosition(Vector2 position)
        {
            this.position = position;
        }


        public override void Interact()
        {
            Log.OutputResults();
            Log.stepsTaken = 0;
            MapManager.GenerateNewMap();
        }


        public override void Draw(GameTime gametime, SpriteBatch spriteBatch)
        {
            animations.Find(a => a.state == state && a.direction == direction).Draw(gametime, spriteBatch, texture, position, frameSize);
        }
    }
}
