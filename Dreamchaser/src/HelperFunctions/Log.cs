﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Dreamchaser
{
    static class Log
    {
        public static int stepsTaken = 0;
        public static int manhattanDistance = 0;
        public static int shortestPath = 0;
        public static Vector2 playerStart = new Vector2();
        public static Vector2 exitPosition = new Vector2();

        public static void CalcManhattanDistance() // calculates the Manhattan distance between the entrance and exit of a level.
        {
            manhattanDistance = (int)((Math.Abs(playerStart.X - exitPosition.X) + Math.Abs(playerStart.Y - exitPosition.Y)) / Tile.frameSize.X);
        }

        public static void FindShortestPath() // executes the A* algorithm to find the shortest path.
        {
            float[,] walkables = new float[Map.WIDTH, Map.HEIGHT];

            for (int x = 0; x < Map.WIDTH; x++)
            {
                for (int y = 0; y < Map.HEIGHT; y++)
                {
                    walkables[x, y] = (MapManager.map.tiles[y][x].isWalkable && MapManager.map.GetObjectAtPosition(new Vector2(x, y)) == null) ? 1 : 0;
                }
            }

            Point s = new Point((int)Math.Floor(playerStart.X / Tile.frameSize.X),  (int)Math.Floor(playerStart.Y / Tile.frameSize.Y));
            Point e = new Point((int)Math.Floor(exitPosition.X / Tile.frameSize.X), (int)Math.Floor(exitPosition.Y / Tile.frameSize.Y));
            Grid  g = new Grid(Map.WIDTH, Map.HEIGHT, walkables);

            List<Point> p = Pathfinding.FindPath(g, s, e);

            shortestPath = p.Count;

            if (shortestPath == 0)
            {
                MapManager.GenerateNewMap();
            }
        }

        public static void OutputResults() // Outputs results after a level has ended, either to a text file or the console.
        {
            using (StreamWriter sw = File.AppendText("TestResults.txt"))
            {
                sw.WriteLine(manhattanDistance + " " + shortestPath + " " + stepsTaken);
                sw.Close();

                Console.WriteLine(File.ReadAllText("TestResults.txt"));
            }
            /*Console.WriteLine("The player has taken " + stepsTaken + " steps.");
            Console.WriteLine("The optimal path is " + shortestPath + " steps.");
            Console.WriteLine("The Manhattan Distance to the exit is " + manhattanDistance + " steps.");*/
        }
    }
}
